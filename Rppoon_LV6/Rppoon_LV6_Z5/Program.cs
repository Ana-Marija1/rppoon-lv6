﻿using System;

namespace Rppoon_LV6_Z5
{
    

    class Program
    {

        static void Main(string[] args)
        {
            //Zadatak 5.
            //Dati implementaciju metoda gdje ona nedostaje u primjeru 3 te ga testirati doradom teksta programa danog
            //u nastavku.Metoda WriteMessage() u klasi FileLogger treba uz poruku zapisati i njen tip i vrijeme zapisa.

            AbstractLogger logger = new ConsoleLogger(MessageType.ALL);
            FileLogger fileLogger = new FileLogger(MessageType.ERROR | MessageType.WARNING, "D:\\Ana-Marija\\Faks\\IV. semestar\\RPPOON\\Rppoon_LV6\\Rppoon_LV6_Z5\\logFile.txt");

            logger.SetNextLogger(fileLogger);

            logger.Log("Message one", MessageType.ERROR);
            logger.Log("Message two", MessageType.INFO);
            logger.Log("Message three", MessageType.WARNING);

        }
    }
}
