﻿using System;

namespace Rppoon_LV6_Z2
{
    class Program
    {
        static void Main(string[] args)
        {
            //Zadatak 2.
            //Po uzoru na primjer 1 ostvariti iterator za potrebe klasa danih u nastavku.

            Box box = new Box();

            box.AddProduct(new Product("White Bread Flour", 9.99));
            box.AddProduct(new Product("Ramen Chicken Noodles", 3.50));
            box.AddProduct(new Product("Butter", 5.50));
            box.AddProduct(new Product("Baked Beans In Tomato Sauce", 7.50));

            box.RemoveProduct(box[3]);

            IAbstractIterator iterator = box.GetIterator();

            for(Product product = iterator.First(); iterator.IsDone == false; product = iterator.Next())
            { 
                Console.WriteLine(product.ToString());
            }
        }
    }
}
