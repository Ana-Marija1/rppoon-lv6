﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rppoon_LV6_Z2
{
    interface IAbstractCollection
    {
        IAbstractIterator GetIterator();
    }
}
