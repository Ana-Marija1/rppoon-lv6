﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rppoon_LV6_Z7
{
    class PasswordValidator
    {
        private StringChecker firstLink;
        private StringChecker lastLink;

        public PasswordValidator(StringChecker first)
        {
            this.firstLink = first;
            this.lastLink = first;
        }

        public void AddNewLink(StringChecker newLink)
        {
            this.lastLink.SetNext(newLink);
            this.lastLink=newLink;
        }

        public bool CheckPassword(String password)
        {
            return firstLink.Check(password);
        }
    }
}
