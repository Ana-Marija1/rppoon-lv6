﻿using System;

namespace Rppoon_LV6_Z7
{
    class Program
    {
        static void Main(string[] args)
        {
            //Zadatak 7.
            //Napisati klasu PasswordValidator koja se koristiti lancem odgovornosti iz zadatka 6 za provjeru valjanosti
            //lozinke.Klasa treba pružiti metodu za dodavanje karika u lanac, a preko konstruktora joj se daje samo prva
            //u redu.

            StringChecker checker = new StringLengthChecker();
            StringDigitChecker digitChecker = new StringDigitChecker();
            StringLowerCaseChecker lowerCaseChecker = new StringLowerCaseChecker();
            StringUpperCaseChecker upperCaseChecker = new StringUpperCaseChecker();

            PasswordValidator passwordValidator = new PasswordValidator(checker);

            passwordValidator.AddNewLink(digitChecker);
            passwordValidator.AddNewLink(lowerCaseChecker);
            passwordValidator.AddNewLink(upperCaseChecker);


            if (passwordValidator.CheckPassword("pDbf5Zg8"))
            {
                Console.WriteLine("First password passes all checks!");
            }
            else
            {
                Console.WriteLine("First password does not pass all checks!");
            }
            if (passwordValidator.CheckPassword("password2"))
            {
                Console.WriteLine("Second password passes all checks!");
            }
            else
            {
                Console.WriteLine("Second password does not pass all checks!");
            }
        }
    }
}
