﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Rppoon_LV6_Z3
{
    class CareTaker
    {
        List<Memento> mementos = new List<Memento>();
        public void AddState(Memento memento)
        {
            this.mementos.Add(memento);
        }
        public Memento GetState(int memento)
        {
            if(memento>mementos.Count-1 || memento < 0)
            {
                Console.WriteLine("There is no to do items!");
                memento = mementos.Count - 1;
            }

            return mementos[memento];
        }
        public void RemoveState(int memento)
        {
            this.mementos.RemoveAt(memento);
        }
    }
}
