﻿using System;

namespace Rppoon_LV6_Z3
{
    class Program
    {
        static void Main(string[] args)
        {
            //Zadatak 3.
            //Izmijeniti klasu CareTaker iz primjera 2 tako da može čuva više stanja(primjerice, unutar popisa) te testirati
            //klase iz primjera.

            DateTime timeDue1 = new DateTime(2020, 5, 28, 7, 11, 11);
            DateTime timeDue2 = new DateTime(2020, 5, 24, 12, 30, 0);

            ToDoItem toDo1 = new ToDoItem("Household chores", "Do laundry",timeDue1);
            ToDoItem toDo2 = new ToDoItem("Household chores", "Cook lunch",timeDue2);

            CareTaker careTaker = new CareTaker();
           
            careTaker.AddState(toDo1.StoreState());
            Console.WriteLine(toDo1.ToString());

            toDo1.ChangeTimeDue(timeDue2);
            toDo1.ChangeTask("Iron the laundry");
            careTaker.AddState(toDo1.StoreState());
            Console.WriteLine(toDo1.ToString());

            toDo1.RestoreState(careTaker.GetState(0));
            Console.WriteLine(toDo1.ToString());
            careTaker.RemoveState(1);
            Console.WriteLine();

            careTaker.AddState(toDo2.StoreState());
            Console.WriteLine(toDo2.ToString());
            toDo2.Rename("Cooking");
            Console.WriteLine(toDo2.ToString());

        }
    }
}
