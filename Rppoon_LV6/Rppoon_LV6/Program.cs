﻿using System;

namespace Rppoon_LV6
{
    class Program
    {
        static void Main(string[] args)
        {
            //Zadatak 1.
            //U primjeru 1 implementirati metode koje to već nisu. Potom, testirati ga – koristiti petlju po vlastitom izboru.

            Notebook notebook = new Notebook();
            
            notebook.AddNote(new Note("Household chores", "Iron the laundry"));
            notebook.Clear();

            notebook.AddNote(new Note("Call", "Do not forget to call your mom!"));
            notebook.AddNote(new Note("Groceries list", "Butter"));
            notebook.AddNote(new Note("Household chores", "Do laundry"));
            notebook.AddNote(new Note("Homework", "Do the assignment"));

            notebook.RemoveNote(notebook[2]);

            IAbstractIterator iterator = notebook.GetIterator();

            for(Note note = iterator.First(); iterator.IsDone == false; note = iterator.Next())
            {                
                note.Show();
            }
            
        }
    }
}
