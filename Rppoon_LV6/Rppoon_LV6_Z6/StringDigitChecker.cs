﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rppoon_LV6_Z6
{
    class StringDigitChecker:StringChecker
    {

        protected override bool PerformCheck(string stringToCheck)
        {
            bool hasDigit = false;
            foreach(char symbol in stringToCheck)
            {
                if (char.IsDigit(symbol))
                {
                    hasDigit = true;
                }
            }
            return hasDigit;
        }
    }
}
