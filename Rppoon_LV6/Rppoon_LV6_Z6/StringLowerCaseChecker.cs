﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rppoon_LV6_Z6
{
    class StringLowerCaseChecker:StringChecker
    {
        protected override bool PerformCheck(string stringToCheck)
        {
            bool hasLower = false;
            foreach (char symbol in stringToCheck)
            {
                if (char.IsLower(symbol))
                {
                    hasLower = true;
                }
            }
            return hasLower;
        }
    }
}
