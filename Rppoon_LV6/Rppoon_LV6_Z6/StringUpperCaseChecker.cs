﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rppoon_LV6_Z6
{
    class StringUpperCaseChecker:StringChecker
    {
        protected override bool PerformCheck(string stringToCheck)
        {
            bool hasUpper = false;
            foreach (char symbol in stringToCheck)
            {
                if (char.IsUpper(symbol))
                {
                    hasUpper = true;
                }
            }
            return hasUpper;
        }
    }
}
