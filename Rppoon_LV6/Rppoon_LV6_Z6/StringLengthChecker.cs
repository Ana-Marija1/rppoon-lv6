﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rppoon_LV6_Z6
{
    class StringLengthChecker:StringChecker
    {
        private int minLength;
        public StringLengthChecker()
        {
            this.minLength = 8;
        }
        protected override bool PerformCheck(string stringToCheck)
        {
            return stringToCheck.Length >= this.minLength;
        }
    }
}
