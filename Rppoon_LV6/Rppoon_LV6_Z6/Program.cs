﻿using System;

namespace Rppoon_LV6_Z6
{
    class Program
    {
        static void Main(string[] args)
        {
            //Zadatak 6.
            //Iz klase StringChecker(dan u nastavku) koja predstavlja osnovni postupak provjere stringa, izvesti klase
            //StringDigitChecker, StringLengthChecker, StringUpperCaseChecker i StringLowerCaseChecker. One trebaju
            //redom omogućiti provjeru sadrži li string barem jednu znamenku, barem jedno veliko i barem jedno malo
            //slovo.Prikazati njihovo ulančavanje i testirati ih.

            StringChecker checker = new StringLengthChecker();
            StringDigitChecker digitChecker = new StringDigitChecker();
            StringLowerCaseChecker lowerCaseChecker = new StringLowerCaseChecker();
            StringUpperCaseChecker upperCaseChecker = new StringUpperCaseChecker();

            checker.SetNext(digitChecker);
            digitChecker.SetNext(lowerCaseChecker);
            lowerCaseChecker.SetNext(upperCaseChecker);



            if (checker.Check("pDbf5Zg8"))
            {
                Console.WriteLine("First password passes all checks!");
            }
            else
            {
                Console.WriteLine("First password does not pass all checks!");
            }
            if (checker.Check("password2"))
            {
                Console.WriteLine("Second password passes all checks!");
            }
            else
            {
                Console.WriteLine("Second password does not pass all checks!");
            }

        }
    }
}
