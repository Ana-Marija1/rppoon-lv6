﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rppoon_LV6_Z4
{
    class Memento
    {
        public string OwnerName { get; private set; }
        public string OwnerAddress { get; private set; }
        public decimal Balance { get; private set; }

        public Memento(string ownerName,string ownerAdderss, decimal balance)
        {
            this.OwnerName = ownerName;
            this.OwnerAddress = ownerAdderss;
            this.Balance = balance;
        }
    } 
}
