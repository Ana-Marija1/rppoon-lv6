﻿using System;
using System.Collections.Generic;

namespace Rppoon_LV6_Z4
{
    class Program
    {
        static void Main(string[] args)
        {

            //Zadatak 4.
            //Po uzoru na primjer 2 ostvariti memento za potrebe klase dane u nastavku te obaviti testiranje.
            List<Memento> careTaker = new List<Memento>();
            BankAccount bankAccount = new BankAccount("Dorris Harvey", "5th Street 34", (decimal)100.25);
            careTaker.Add(bankAccount.StoreState());
            Console.WriteLine(bankAccount.ToString());

            bankAccount.ChangeOwnerAddress("6th Street 567");
            bankAccount.UpdateBalance((decimal)200.99);
            careTaker.Add(bankAccount.StoreState());
            Console.WriteLine(bankAccount.ToString());

            bankAccount.RestoreState(careTaker[0]);
            Console.WriteLine(bankAccount.ToString());
        }
    }
}
